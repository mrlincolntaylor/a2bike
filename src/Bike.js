import React, { Component } from 'react';

import './Bike.css';

class Bike extends Component {
  componentDidMount() {
    document.querySelector(".pause").addEventListener('click', function() {
      document.querySelector(".bike").classList.toggle("paused");
      document.querySelector(".pause").classList.toggle("active");
    });
  }
  render() {
    return (
      <div>
      <div className="pause">
  
</div>
<div className="bike">
  <div className="part frame">
    <div className="bar left-top"></div>
    <div className="bar left-bottom"></div>
    <div className="bar left"></div>
    <div className="bar top"></div>
    <div className="bar bottom"></div>
    <div className="bar right"></div>
  </div>
  <div className="part sadle">
    <div className="sit-here"></div>
    <div className="sadlepen"></div>
  </div>
  <div className="part handlebar">
    <div className="stem"></div>
    <div className="connector"></div>
    <div className="prehandle"></div>
    <div className="handle"></div>
  </div>
  <div className="part pedals">
    <div className="inside"></div>
    <div className="outside"></div>
    <div className="pedalstem front">
      <div className="pedalbase front"></div>
    </div>
    <div className="pedalstem back">
      <div className="pedalbase back"></div>
    </div>
  </div>
  <div className="part wheel left">
    <div className="spoke"></div>
    <div className="spoke"></div>
    <div className="spoke"></div>
    <div className="spoke"></div>
    <div className="spoke"></div>
    <div className="spoke"></div>
    <div className="spoke"></div>
    <div className="spoke"></div>
  </div>
  <div className="part wheel right">
    <div className="spoke"></div>
    <div className="spoke"></div>
    <div className="spoke"></div>
    <div className="spoke"></div>
    <div className="spoke"></div>
    <div className="spoke"></div>
    <div className="spoke"></div>
    <div className="spoke"></div>
  </div>
  <div className="part axis left"></div>
  <div className="part axis right"></div>
</div>

<div className="bike bike-two">
  <div className="part frame">
    <div className="bar left-top"></div>
    <div className="bar left-bottom"></div>
    <div className="bar left"></div>
    <div className="bar top"></div>
    <div className="bar bottom"></div>
    <div className="bar right"></div>
  </div>
  <div className="part sadle">
    <div className="sit-here"></div>
    <div className="sadlepen"></div>
  </div>
  <div className="part handlebar">
    <div className="stem"></div>
    <div className="connector"></div>
    <div className="prehandle"></div>
    <div className="handle"></div>
  </div>
  <div className="part pedals">
    <div className="inside"></div>
    <div className="outside"></div>
    <div className="pedalstem front">
      <div className="pedalbase front"></div>
    </div>
    <div className="pedalstem back">
      <div className="pedalbase back"></div>
    </div>
  </div>
  <div className="part wheel left">
    <div className="spoke"></div>
    <div className="spoke"></div>
    <div className="spoke"></div>
    <div className="spoke"></div>
    <div className="spoke"></div>
    <div className="spoke"></div>
    <div className="spoke"></div>
    <div className="spoke"></div>
  </div>
  <div className="part wheel right">
    <div className="spoke"></div>
    <div className="spoke"></div>
    <div className="spoke"></div>
    <div className="spoke"></div>
    <div className="spoke"></div>
    <div className="spoke"></div>
    <div className="spoke"></div>
    <div className="spoke"></div>
  </div>
  <div className="part axis left"></div>
  <div className="part axis right"></div>

</div>
</div>
    );
  }
}

export default Bike;
