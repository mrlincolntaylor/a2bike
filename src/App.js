import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Bike from './Bike'
import Planner from './form';


class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <Bike/> 
        </header>
        <p className="App-intro">
        <h1 className="App-title">A2Bike</h1>
          <h2 className="App-intro">Reach your destination</h2>
          Welcome to A2Bike, the helpful tool to display popular start/end points, journey durations and get you round the City!
        </p>
        <Planner />
      </div>
    );
  }
}

export default App;
